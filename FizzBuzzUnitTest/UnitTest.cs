using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzzKata;

namespace FizzBuzzUnitTest
{
    [TestClass]
    public class UnitTest
    {
        [DataRow(0, "FizzBuzz")]
        [DataRow(-5, "Buzz")]
        [DataRow(-3, "Fizz")]
        [DataRow(3, "Fizz")]
        [DataRow(5, "Buzz")]
        [DataRow(2, "2")]
        [DataRow(30, "FizzBuzz")]
        [DataRow(15, "FizzBuzz")]
        [DataRow(7, "7")]
        [DataRow(8, "8")]
        [DataRow(67, "67")]
        [DataTestMethod]
        public void FizzBuzzTest(int value, string expectedResult) {
            //Arrange
            FizzBuzzKata.FizzBuzz fizzBuzz = new FizzBuzz();
            string fizzBuzzResult;

            //Act
            fizzBuzzResult = fizzBuzz.Process(value);

            //Assert
            Assert.AreEqual(expectedResult, fizzBuzzResult);
        }
    }
}
