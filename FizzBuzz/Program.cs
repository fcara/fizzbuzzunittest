﻿using System;

namespace FizzBuzzKata
{
    class Program
    {
        static void Main(string[] args) {
            FizzBuzz fizzBuzz = new FizzBuzz();
            for (int i = 0; i < 20; i++) {
                Console.WriteLine(fizzBuzz.Process(i));
            }
        }
    }
}
